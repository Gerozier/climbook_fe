import {Component, OnInit} from '@angular/core';
import {Area} from '../_interface/area';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../_service/security.service';
import {User} from '../_interface/user';
import {DialogService} from '../_service/dialog.service';
import {DateService} from '../_service/date.service';
import {ParamsService} from '../_service/params.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {
  sessionUser: User;
  areas: Area[];

  constructor(private dialogService: DialogService,
              private router: Router,
              private paramsService: ParamsService,
              private dateService: DateService,
              private securityService: SecurityService,
              private httpClient: HttpClient) {
    this.sessionUser = JSON.parse(localStorage.getItem('climbookUser'));
  }

  ngOnInit(): void {
    this.getAllAreas();
  }

  getAllAreas() {
    this.httpClient.get<Area[]>('/api/v1/areas')
      .subscribe(response => {
        if (response != null) {
          response.forEach(area => {
            area.date = this.dateService.formattime(area.createDate);
          });
        }
        this.areas = response;
      });
  }

  public createArea(form: Area) {
    form.createDate = Math.floor(Date.now() / 1000);
    this.httpClient.post<Area>('/api/v1/create/areas', form)
      .subscribe(() => {
          this.getAllAreas();
          this.dialogService.openDialog('Erfolgreich erstellt', 'Das Gebiet ' + form.name + ' wurde erstellt');
        },
        () => {
            this.dialogService.openDialog('Fehler bei der Erstellung', 'Das Gebiet enthält keinen Namen');
        }
      );
  }

  public deleteArea(clientId: string, name: string) {
    this.httpClient.delete<Area>('/api/v1/areas/' + clientId)
      .subscribe(() => {
          this.getAllAreas();
          this.dialogService.openDialog('Erfolgreich gelöscht', 'Das Gebiet ' + name + ' wurde gelöscht');
        },
        () => this.dialogService.openDialog('Fehler bei Löschen', 'Das Gebiet konnte nicht gelöscht werden')
      );
  }

  public saveParam(areaid: string, areaname: string) {
    this.router.navigate([areaname]);
    this.paramsService.areaparam = areaid;
  }

}

