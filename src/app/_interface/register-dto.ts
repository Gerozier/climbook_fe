export interface RegisterDTO {
  name: string;
  password1: string;
  password2: string;
}
