export class Route {
  name: string;
  clientId: string;
  description: string;
  difficulty: string;
  complete: boolean;
  createDate: number;
  lastUpdate: number;
  date: string;
  update: string;
}
