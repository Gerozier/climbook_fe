export class Area {
  name: string;
  clientId: string;
  description: string;
  createDate: number;
  date: string;
}
