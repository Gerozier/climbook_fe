export interface ProfileDTO {
  name: string;
  email: string;
  rocks: [];
  routes: [];
}
