export class Rock {
  name: string;
  clientId: string;
  description: string;
  complete: boolean;
  createDate: number;
  date: string;
}
