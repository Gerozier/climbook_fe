import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Area} from '../_interface/area';
import {Rock} from '../_interface/rock';
import {User} from '../_interface/user';
import {SecurityService} from '../_service/security.service';
import {DialogService} from '../_service/dialog.service';
import {DateService} from '../_service/date.service';
import {ParamsService} from '../_service/params.service';
import {RockDTO} from '../_interface/rock-dto';


@Component({
  selector: 'app-areadetail',
  templateUrl: './areadetail.component.html',
  styleUrls: ['./areadetail.component.css']
})
export class AreadetailComponent implements OnInit {

  sessionUser: User;
  rocksdto: RockDTO[];

  actualArea = {
    name: '',
    description: ''
  };

  constructor(private dialogService: DialogService,
              private dateService: DateService,
              private router: Router,
              private route: ActivatedRoute,
              private paramsService: ParamsService,
              private securityService: SecurityService,
              private httpClient: HttpClient) {
    this.sessionUser = JSON.parse(localStorage.getItem('climbookUser'));
  }

  ngOnInit(): void {
    if (this.sessionUser != null) {
      this.getArea();
      this.getAllRocksByAreaId();
    } else {
      this.getAreaUnregistered();
      this.getAllRocksByAreaIdUnregistered();
    }
  }

  getArea(): void {
    this.httpClient.get<Area>('/api/v1/areas/' + this.paramsService.areaparam)
      .subscribe(response => this.actualArea = response);
  }

  updateArea() {
    this.httpClient.put('api/areas/' + this.paramsService.areaparam, this.actualArea)
      .subscribe(() => {
          this.dialogService.openDialog('Erfolgreich bearbeitet', 'Das Gebiet ' + this.actualArea.name + ' wurde erfolgreich bearbeitet');
        },
        (err: HttpErrorResponse) => {
          if (err.status === 400) {
            this.dialogService.openDialog('Fehler bei der Bearbeitung', 'Das Gebiet enthält keinen Namen');
          } else if (err.status === 404) {
            this.dialogService.openDialog('Fehler bei der Bearbeitung', 'Das Gebiet ' + this.actualArea.name +
              ' konnte nicht bearbeitet werden da es nicht existiert. Bitte erstellen Sie erst ein Gebiet mit diesem Namen.');
          }
          this.getArea();
        }
      );
  }

  getAllRocksByAreaId() {
    this.httpClient.get<RockDTO[]>('/api/v1/area/rocks/' + this.paramsService.areaparam)
      .subscribe(response => {
        if (response != null) {
          response.forEach(rock => {
            rock.date = this.dateService.formattime(rock.createDate);
          });
        }
        this.rocksdto = response;
      });
  }

  public createRock(form: any) {
    this.httpClient.post<Rock>('/api/v1/area/rocks/' + this.paramsService.areaparam, form)
      .subscribe(() => {
          this.getAllRocksByAreaId();
          this.dialogService.openDialog('Erfolgreich erstellt', 'Der Fels ' + form.name + ' wurde erstellt');

        },
        () => this.dialogService.openDialog('Fehler bei der Erstellung', 'Bitte geben sie einen gültigen Namen ein.')
      );
  }

  public deleteRock(clientId: string, name: string) {
    this.httpClient.delete<Rock>('/api/v1/rocks/' + clientId)
      .subscribe(() => {
          this.getAllRocksByAreaId();
          this.dialogService.openDialog('Erfolgreich gelöscht', 'Der Fels ' + name + ' wurde gelöscht');
        },
        () => this.dialogService.openDialog('Fehler bei Löschen', 'Der Fels konnte nicht gelöscht werden')
      );
  }

  public saveParam(id: string, areaname: string, rockname: string) {
    this.paramsService.areaname = areaname;
    this.router.navigate([areaname + '/' + rockname]);
    this.paramsService.rockparam = id;
  }

  getAllRocksByAreaIdUnregistered() {
    this.httpClient.get<Rock[]>('/api/v1/unregistered/area/rocks/' + this.paramsService.areaparam)
      .subscribe(response => {
        this.rocksdto = response;
      });
  }

  getAreaUnregistered(): void {
    this.httpClient.get<Area>('/api/v1/unregistered/areas/' + this.paramsService.areaparam)
      .subscribe(response => this.actualArea = response);
  }
}
