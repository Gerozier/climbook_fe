import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../_service/security.service';
import {RegisterDTO} from '../_interface/register-dto';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private securityService: SecurityService) { }

  ngOnInit(): void {
  }

  register(form: RegisterDTO) {
    this.securityService.register(form);
  }

}
