import {Component, OnInit} from '@angular/core';
import {Route} from '../_interface/route';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {User} from '../_interface/user';
import {SecurityService} from '../_service/security.service';
import {DialogService} from '../_service/dialog.service';
import {ParamsService} from '../_service/params.service';

@Component({
  selector: 'app-routedetail',
  templateUrl: './routedetail.component.html',
  styleUrls: ['./routedetail.component.css']
})
export class RoutedetailComponent implements OnInit {

  sessionUser: User | null = null;

  actualRoute = {
    name: '',
    description: '',
    difficulty: '',
    complete: false
  };

  constructor(private dialogService: DialogService,
              private paramsService: ParamsService,
              private route: ActivatedRoute,
              private securityService: SecurityService,
              private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.securityService.getSessionUser().subscribe(
      response => this.sessionUser = response
    );
    if (this.sessionUser != null) {
      this.getRoute();
    } else {
      this.getRouteUnregistered();
    }
  }

  getRoute(): void {
    this.httpClient.get<Route>('/api/v1/routes/' + this.paramsService.routeparam)
      .subscribe(response => this.actualRoute = response);
  }

  updateRoute() {
    this.httpClient.put('api/v1/routes/' + this.paramsService.routeparam, this.actualRoute).subscribe(() =>
        {
          this.dialogService.openDialog('Erfolgreich bearbeitet', 'Die Route ' + this.actualRoute.name + ' wurde erfolgreich bearbeitet');
        },
      () => {
        this.getRoute();
        this.dialogService.openDialog('Fehler bei der Bearbeitung', 'Die Route enthält keinen Namen');
      }
    );
  }

  public setRouteDoneOrUndone() {
    this.httpClient.post<Route[]>('/api/v1/routes/' + this.paramsService.routeparam, null)
      .subscribe(() => {
          this.getRoute();
          if (!this.actualRoute.complete) {
            this.dialogService.openDialog('Erfolgreich markiert', 'Die Route wurde als erledigt markiert');
          } else {
            this.dialogService.openDialog('Markierung entfernt', 'Die "Erledigt"-Markierung wurde entfernt');
          }
        },
        () => this.dialogService.openDialog('Fehler bei der Markierung', 'Die Route konnte nicht als erledigt markiert werden')
      );
  }


  getRouteUnregistered(): void {
    this.httpClient.get<Route>('/api/v1/unregistered/routes/' + this.paramsService.routeparam)
      .subscribe(response => this.actualRoute = response);
  }

}
