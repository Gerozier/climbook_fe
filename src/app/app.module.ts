import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {RoutedetailComponent} from './routedetail/routedetail.component';
import {RockdetailComponent} from './rockdetail/rockdetail.component';
import {AreaComponent} from './area/area.component';
import {AreadetailComponent} from './areadetail/areadetail.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatSidenavModule} from '@angular/material/sidenav';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';
import {LandingpageComponent} from './landingpage/landingpage.component';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {DialogComponent} from './dialog/dialog.component';
import {AdminuseroverviewComponent} from './adminuseroverview/adminuseroverview.component';
import {RegisterComponent} from './register/register.component';
import {ProfileComponent} from './profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    RoutedetailComponent,
    AreaComponent,
    RockdetailComponent,
    AreadetailComponent,
    LoginComponent,
    LandingpageComponent,
    DialogComponent,
    AdminuseroverviewComponent,
    RegisterComponent,
    ProfileComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        MatSidenavModule,
        FormsModule,
        MatSelectModule,
        MatExpansionModule,
        MatDialogModule,
        HttpClientXsrfModule.withOptions({cookieName: 'XSRF-TOKEN'})
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
