import {Injectable} from '@angular/core';
import {DialogComponent} from '../dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog) { }

  openDialog(title: string, message: string) {
    this.dialog.open(DialogComponent, {
      width: '250px',
      data: {title, message}
    });
  }
}
