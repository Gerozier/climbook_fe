import {Injectable} from '@angular/core';
import {User} from '../_interface/user';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {RegisterDTO} from '../_interface/register-dto';
import {DialogService} from './dialog.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  public sessionUser = new BehaviorSubject<User | null>(null);

  constructor(private httpClient: HttpClient,
              private dialogService: DialogService,
              private router: Router) {
    this.httpClient.get<User>('/api/v1/sessionuser').subscribe(
      response => this.sessionUser.next(response)
    );
  }

  public getSessionUser(): Observable<User | null> {
    return this.sessionUser;
  }

  public login(form: any) {
    this.httpClient.get<User>('/api/v1/sessionuser', {
      headers: {
        authorization: 'Basic ' + btoa(form.username + ':' + form.password)
      }
    }).subscribe(
      response => {
        this.sessionUser.next(response);
        this.router.navigateByUrl('/');
        localStorage.setItem('climbookUser', JSON.stringify(response));
      },
      (err: HttpErrorResponse) => {
        if (err.status === 403) {
          this.dialogService.openDialog('Fehler beim Einloggen', 'Benutzername und Passwort stimmen nicht überein.');
        }
        this.sessionUser.next(null);
      }
    );
  }

  public register(user: RegisterDTO) {
    this.httpClient.post<User>('/api/v1/register/user', user)
      .subscribe(
        () => {
          this.router.navigateByUrl('/login');
        }, (err: HttpErrorResponse) => {
          if (err.status === 406) {
            this.dialogService.openDialog('Fehler bei der Registrierung', 'Dieser Benutzername ist bereits vergeben');
          } else if (err.status === 400) {
            this.dialogService.openDialog('Fehler bei der Registrierung', 'Die Passwörter stimmen nicht überein');
          }else if (err.status === 409) {
            this.dialogService.openDialog('Fehler bei der Registrierung', 'Bitte geben sie eine korrekte E-Mail Adresse an');
          }
        }
      );
  }

  public logout() {
    this.httpClient.post('/api/v1/logout', null).subscribe(
      () => this.sessionUser.next(null),
    );
    localStorage.removeItem('climbookUser');
  }
}
