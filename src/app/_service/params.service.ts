import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParamsService {

  areaparam: string;
  rockparam: string;
  routeparam: string;
  areaname: string;

  constructor() { }
}
