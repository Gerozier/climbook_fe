import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  formattime(tick: number) {
    const date = new Date(tick * 1000);
    return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
  }
}
