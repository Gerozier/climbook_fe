import {Component, OnInit} from '@angular/core';
import {User} from '../_interface/user';
import {SecurityService} from '../_service/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  sessionUser: User|null = null;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      response => this.sessionUser = response
    );
  }

  login(form: any) {
    this.securityService.login(form);
  }
}
