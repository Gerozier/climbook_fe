import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoutedetailComponent} from './routedetail/routedetail.component';
import {AreaComponent} from './area/area.component';
import {RockdetailComponent} from './rockdetail/rockdetail.component';
import {AreadetailComponent} from './areadetail/areadetail.component';
import {LoginComponent} from './login/login.component';
import {LandingpageComponent} from './landingpage/landingpage.component';
import {AdminuseroverviewComponent} from './adminuseroverview/adminuseroverview.component';
import {RegisterComponent} from './register/register.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  {path: ':areaname/:rockname/:routename', component: RoutedetailComponent},
  {path: '', component: LandingpageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'useroverview', component: AdminuseroverviewComponent},
  {path: ':areaname/:rockname', component: RockdetailComponent},
  {path: 'areas', component: AreaComponent},
  {path: 'profile', component: ProfileComponent},
  {path: ':areaname', component: AreadetailComponent}
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
