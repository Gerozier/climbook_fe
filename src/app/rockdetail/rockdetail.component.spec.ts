import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RockdetailComponent } from './rockdetail.component';

describe('RockdetailComponent', () => {
  let component: RockdetailComponent;
  let fixture: ComponentFixture<RockdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RockdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RockdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
