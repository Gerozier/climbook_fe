import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Rock} from '../_interface/rock';
import {Route} from '../_interface/route';
import {SecurityService} from '../_service/security.service';
import {User} from '../_interface/user';
import {DialogService} from '../_service/dialog.service';
import {DateService} from '../_service/date.service';
import {ParamsService} from '../_service/params.service';

@Component({
  selector: 'app-rockdetail',
  templateUrl: './rockdetail.component.html',
  styleUrls: ['./rockdetail.component.css']
})
export class RockdetailComponent implements OnInit {

  sessionUser: User | null = null;
  routes: Route[];

  actualRock = {
    name: '',
    description: '',
    complete: false
  };

  constructor(private dialogService: DialogService,
              private dateService: DateService,
              private router: Router,
              private route: ActivatedRoute,
              private paramsService: ParamsService,
              private securityService: SecurityService,
              private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      response => this.sessionUser = response
    );
    if (this.sessionUser != null) {
      this.getRock();
      this.getAllRoutesByRockId();
    } else {
      this.getRockUnregistered();
      this.getAllRoutesByRockIdUnregistered();
    }
  }


  public saveParam(id: string, rockname: string, routename: string) {
    this.router.navigate([this.paramsService.areaname + '/' + rockname + '/' + routename]);
    this.paramsService.routeparam = id;
  }

  getRock() {
    this.httpClient.get<Rock>('/api/v1/rocks/' + this.paramsService.rockparam)
      .subscribe(response => this.actualRock = response);
  }

  getAllRoutesByRockId() {
    this.httpClient.get<Route[]>('/api/v1/rock/routes/' + this.paramsService.rockparam)
      .subscribe(response => {
        if (response != null) {
          response.forEach(route => {
            route.date = this.dateService.formattime(route.createDate);
            route.update = this.dateService.formattime(route.lastUpdate);
          });
        }
        this.routes = response;
      });
  }

  updateRock() {
    this.httpClient.put('api/rocks/' + this.paramsService.rockparam, this.actualRock)
      .subscribe(() => {
        this.dialogService.openDialog('Erfolgreich bearbeitet', 'Der Fels ' + this.actualRock.name + ' wurde erfolgreich bearbeitet');
      },
      () => {
        this.getRock();
        this.dialogService.openDialog('Fehler bei der Bearbeitung', 'Der Fels enthält keinen Namen');
      }
    );
  }

  public createRoute(form: any) {
    this.httpClient.post<Route>('/api/v1/rock/routes/' + this.paramsService.rockparam, form)
      .subscribe(() => {
        this.getAllRoutesByRockId();
        this.dialogService.openDialog('Erfolgreich erstellt', 'Die Route ' + form.name + ' wurde erstellt');
      },
      () => this.dialogService.openDialog('Fehler bei der Erstellung', 'Die Route enthält keinen Namen')
    );
  }

  public deleteRoute(clientId: string, name: string) {
    this.httpClient.delete<Rock>('/api/v1/routes/' + clientId)
      .subscribe(() => {
        this.getAllRoutesByRockId();
        this.dialogService.openDialog('Erfolgreich gelöscht', 'Die Route ' + name + ' wurde gelöscht');
      },
      () => this.dialogService.openDialog('Fehler bei Löschen', 'Die Route konnte nicht gelöscht werden')
    );
  }
  public setRockDone() {
    this.httpClient.post<Rock[]>('/api/v1/rocks/' + this.paramsService.rockparam, null)
      .subscribe(() => {
          this.getRock();
          if (!this.actualRock.complete) {
            this.dialogService.openDialog('Erfolgreich markiert', 'Der Felsen ' + this.actualRock.name + ' wurde als erledigt markiert');
          } else {
            this.dialogService.openDialog('Markierung entfernt', 'Die "Erledigt"-Markierung wurde entfernt');
          }
        },
        () => this.dialogService.openDialog('Fehler bei der Markierung', 'Der Felsen konnte nicht als erledigt markiert werden')
      );
  }

  getAllRoutesByRockIdUnregistered() {
    this.httpClient.get<Route[]>('/api/v1/unregistered/rock/routes/' + this.paramsService.rockparam)
      .subscribe(response => this.routes = response);
  }

  getRockUnregistered() {
    this.httpClient.get<Rock>('/api/v1/unregistered/rocks/' + this.paramsService.rockparam)
      .subscribe(response => this.actualRock = response);
  }
}
