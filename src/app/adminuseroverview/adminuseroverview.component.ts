import {Component, OnInit} from '@angular/core';
import {User} from '../_interface/user';
import {ActivatedRoute} from '@angular/router';
import {SecurityService} from '../_service/security.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DialogService} from '../_service/dialog.service';
import {UserDTO} from '../_interface/user-dto';

@Component({
  selector: 'app-adminuseroverview',
  templateUrl: './adminuseroverview.component.html',
  styleUrls: ['./adminuseroverview.component.css']
})
export class AdminuseroverviewComponent implements OnInit {

  sessionUser: User | null = null;

  userDTO: UserDTO[];

  constructor(private dialogService: DialogService,
              private route: ActivatedRoute,
              private securityService: SecurityService,
              private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.securityService.getSessionUser()
      .subscribe(
        response => this.sessionUser = response
      );
    this.getUserOverview();
  }

  getUserOverview(): void {
    this.httpClient.get<UserDTO[]>('/api/v1/user/')
      .subscribe(response => this.userDTO = response);
  }

  public deleteUser(username: string) {
    const params = new HttpParams()
      .set('username', username);
    this.httpClient.delete<User>('/api/v1/user/', {params})
      .subscribe(() => {
          this.getUserOverview();
          this.dialogService.openDialog('Erfolgreich gelöscht', 'Der Benutzer ' + username + ' wurde gelöscht');
        },
        () => this.dialogService.openDialog('Fehler bei Löschen', 'Der Benutzer konnte nicht gelöscht werden')
      );
  }

  changeUserRole(username: string, role: string) {
    const params = new HttpParams()
      .set('username', username)
      .set('role', role);
    this.httpClient.put('api/user/', null, {params})
      .subscribe(() => {
          this.getUserOverview();
          this.dialogService.openDialog('Erfolgreich bearbeitet', 'Die Rolle von ' + username + ' wurde erfolgreich bearbeitet');
        },
        () => {
          this.dialogService.openDialog('Fehler bei der Bearbeitung', 'Die Rolle konnte nicht verändert werden');
        }
      );
  }

}
