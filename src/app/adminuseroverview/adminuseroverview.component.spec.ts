import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminuseroverviewComponent } from './adminuseroverview.component';

describe('AdminuseroverviewComponent', () => {
  let component: AdminuseroverviewComponent;
  let fixture: ComponentFixture<AdminuseroverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminuseroverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminuseroverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
