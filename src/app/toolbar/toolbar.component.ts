import {Component, OnInit} from '@angular/core';
import {User} from '../_interface/user';
import {SecurityService} from '../_service/security.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  sessionUser: User|null;

  constructor(public securityService: SecurityService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      response => this.sessionUser = response
    );
  }

}
