import {Component, OnInit} from '@angular/core';
import {User} from '../_interface/user';
import {SecurityService} from '../_service/security.service';
import {ProfileDTO} from '../_interface/profile-dto';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  sessionUser: User | null = null;

  userProfile: ProfileDTO | null;

  constructor(private securityService: SecurityService,
              private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.securityService.getSessionUser()
      .subscribe(
        response => this.sessionUser = response
      );
    if (this.sessionUser != null) {
      this.getProfile();
      console.log(this.userProfile);
    }
  }

  getProfile() {
    this.httpClient.get<ProfileDTO>('/api/v1/user/profile')
      .subscribe(response => this.userProfile = response);
  }

}
