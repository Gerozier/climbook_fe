# ClimBook Frontend

Projekt Beschreibung

_Syntax: <project_name> is a <utility/tool/feature> that allows <insert_target_audience> to do <action/task_it_does>._

## Inhaltsverzeichnis
1. [GettingStarted](#getting-started)
1. [Benutzung](#usability)
2. [URL's](#urls)
3. [Entwickler](#authors)


<a name="getting-started"></a>
### Getting Started
Anleitung wie man sich eine Kopie des Projektes holt und lokal laufen lässt.


Dieses Projekt wurde erstellt mit [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

#### Voraussetzung
Um das Frontend des Projekts zu installieren, benötigt man folgendes Git Repository:
* [Frontend](https://gitlab.com/Gerozier/climbook_fe.git)

#### Installieren
```
cd  c: ./climbook-fe
cd ng serve
```


<a name="usability"></a>
### Benutzung
Wenn das Backend läuft kann man sich mit folgenden Benutzerdaten anmelden:

User Rechte: TestUser - user

Admin Rechte: TestAdmin - admin

<a name="urls"></a>
### URL's
Um Daten anzuzeigen benötigt man das folgende Backend (Git Repository) und eine laufende MariaDB:
* [Backend](https://gitlab.com/Gerozier/climbook.git)

<a name="authors"></a>
### Entwickler
* Dennis Brämer [Gitlab Profil: Gerozier](https://gitlab.com/Gerozier)
* Mareike Emde [Gitlab Profil: Moinamour](https://gitlab.com/moinamour)

